package roboSoc.Pos;

import roboSoc.Core.IPosicionadorRemoto;
import roboSoc.Core.Posicion;

public class PosicionadorRandomRemoto_ERR implements IPosicionadorRemoto {

	@Override
	public Posicion getPosicion(int id) {
		
		Posicion pos = new Posicion();
		
		// Producir nuevo aleatorio entre 40 y 1000
		double random = 40 + Math.random() * 950;
		pos.set_posX(random);
		
		//Producir nuevo aleatorio entre 20 y 650
		random = 20 + Math.random() * 600;
		pos.set_posY(random);
		
		random = Math.random() * 20;
		pos.set_posZ(random);
		
		return pos;
	}

	@Override
	public Object solicitud(Object param) throws Exception {
		if(((int) (Math.random() * 10))%2 == 0) 
		{
			Exception e = new Exception("Error, sali� PAR");
			throw e;
		}
		
		return this.getPosicion((int) param);
	}

}
