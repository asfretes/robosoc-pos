package roboSoc.Pos;

import java.util.ArrayList;

import roboSoc.Core.IPosicionador;
import roboSoc.Core.Posicion;

public class PosicionadorTXT implements IPosicionador{

	@Override
	public Posicion getPosicion(int id) {
		
		Posicion pos = new Posicion();
				
        switch (id) 
        {
            case 0:  
            	pos.set_posX(45);
            	pos.set_posY(320);
            	break;
            case 1:  
            	pos.set_posX(100);
            	pos.set_posY(100);
            	break;
            case 2:  
            	pos.set_posX(100);
            	pos.set_posY(250);
            	break;
            case 3:  
            	pos.set_posX(100);
            	pos.set_posY(500);
            	break;
            case 4:  
            	pos.set_posX(300);
            	pos.set_posY(200);
            	break;
            case 5:  
            	pos.set_posX(300);
            	pos.set_posY(400);
            	break;
            
            	
            case 6:  
            	pos.set_posX(950);
            	pos.set_posY(320);
            	break;
            case 7:  
            	pos.set_posX(800);
            	pos.set_posY(100);
            	break;
            case 8:  
            	pos.set_posX(800);
            	pos.set_posY(250);
            	break;
            case 9:  
            	pos.set_posX(800);
            	pos.set_posY(500);
            	break;
            case 10:  
            	pos.set_posX(500);
            	pos.set_posY(200);
            	break;
            case 11:  
            	pos.set_posX(500);
            	pos.set_posY(400);
            	break;	
            	
            	
            default: 
            	pos.set_posX(500+id*10);
            	pos.set_posY(300+id*10);
            	break;

        }

       // System.out.println("id: " + id + "  -- X: " + pos.get_posX() + " - Y: " + pos.get_posY()  );
        
        
		return pos;
	}

}
