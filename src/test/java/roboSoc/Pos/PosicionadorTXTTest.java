package roboSoc.Pos;

import static org.junit.Assert.*;

import org.junit.Test;

import roboSoc.Core.IPosicionador;
import roboSoc.Core.Posicion;

public class PosicionadorTXTTest {

	@Test
	public void testGetPosicion() {
		Posicion p1;
		
		IPosicionador pos = new PosicionadorTXT();
	
		p1 = (Posicion) pos.getPosicion(1);
		
		assertTrue(p1.get_posX()==100 
					&& p1.get_posY()==100);
				
	}

}
