package roboSoc.Pos;

import static org.junit.Assert.*;

import org.junit.Test;

import roboSoc.Core.IPosicionador;
import roboSoc.Core.Posicion;

public class PosicionadorRandomTest {

	@Test
	public void testGetPosicion() {
		Posicion p1;
		Posicion p2;
		IPosicionador pos = new PosicionadorRandom();
	
		p1 = (Posicion) pos.getPosicion(1);
		p2 = (Posicion) pos.getPosicion(1);
		
		assertFalse(p1.get_posX()==p2.get_posX() 
					&& p1.get_posZ()==p2.get_posZ());
				
	}

}
